from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import ReceiptForm, CategoryForm, AccountForm


@login_required
def receipt_list(request):
    receipt_list = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list": receipt_list,
    }
    return render(request, "receipts/receipts.html", context)


@login_required
def create_receipt(request):
    # Check if request is a POST
    if request.method == "POST":
        # Create new form instance & fill with values from submission
        form = ReceiptForm(request.POST)
        # Check if the form is valid
        if form.is_valid():
            # save all info from form into variable
            receipt = form.save(False)
            # add logged in user as the purchaser attribute in receipt
            receipt.purchaser = request.user
            # Save receipt with user info
            receipt.save()
        # Redirect to a different page
        return redirect("home")
    else:
        # Create a new form instance
        form = ReceiptForm()
    # Add it to the context
    context = {"form": form}
    return render(request, "receipts/create.html", context)


@login_required
def create_category(request):
    # Check if request is a POST
    if request.method == "POST":
        # Create new form instance & fill with values from submission
        form = CategoryForm(request.POST)
        # Check if the form is valid
        if form.is_valid():
            # save all info from form into variable
            category = form.save(False)
            # add logged in user as the purchaser attribute in receipt
            category.owner = request.user
            # Save receipt with user info
            category.save()
        # Redirect to a different page
        return redirect("categories")
    else:
        # Create a new form instance
        form = CategoryForm()
    # Add it to the context
    context = {"form": form}
    return render(request, "receipts/categories/create.html", context)


@login_required
def create_account(request):
    # Check if request is a POST
    if request.method == "POST":
        # Create new form instance & fill with values from submission
        form = AccountForm(request.POST)
        # Check if the form is valid
        if form.is_valid():
            # save all info from form into variable
            account = form.save(False)
            # add logged in user as the purchaser attribute in receipt
            account.owner = request.user
            # Save receipt with user info
            account.save()
        # Redirect to a different page
        return redirect("accounts")
    else:
        # Create a new form instance
        form = AccountForm()
    # Add it to the context
    context = {"form": form}
    return render(request, "receipts/accounts/create.html", context)


@login_required
def category_list(request):
    # get list of category objects (there aren't objects)
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "categories": categories,
    }
    return render(request, "receipts/categories.html", context)


@login_required
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {
        "accounts": accounts,
    }
    return render(request, "receipts/accounts.html", context)
